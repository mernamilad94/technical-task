import {Component, OnInit} from '@angular/core';
import {faCommentDots, faHome,faUser,faChevronDown,faCog,faBookmark} from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-side-nav',
    templateUrl: './side-nav.component.html',
    styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {
    faHome = faHome;
    faCommentDots = faCommentDots;
    faUser = faUser;
    faChevronDown = faChevronDown;
    faCog = faCog;
    faBookmark = faBookmark;

    constructor() {
    }

    ngOnInit() {
    }

}
