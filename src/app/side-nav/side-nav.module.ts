import {NgModule} from '@angular/core';
import { SideNavComponent } from './side-nav.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";



@NgModule({
    declarations: [
        SideNavComponent

    ],
    imports: [
        FontAwesomeModule
    ],
    exports: [
        SideNavComponent
    ]
})
export class SideNavModule {
}
