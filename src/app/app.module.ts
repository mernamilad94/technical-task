import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {OverlayModule} from '@angular/cdk/overlay';
import {FeedsCardsComponent} from "./feeds-cards/feeds-cards.component";
import {SideNavModule} from "./side-nav/side-nav.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import {CircleCardsModule} from "./circle-cards/circle-cards.module";
import {rightAsideComponent} from "./right-aside/right-aside.component";



@NgModule({
    declarations: [
        AppComponent,
        FeedsCardsComponent,
        rightAsideComponent
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        FormsModule,
        BrowserAnimationsModule,
        OverlayModule,
        SideNavModule,
        FontAwesomeModule,
        CircleCardsModule
    ],
    providers: [],
    bootstrap: [AppComponent],
    exports: []
})
export class AppModule {
}
