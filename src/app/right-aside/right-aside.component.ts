import {Component, OnInit} from '@angular/core';
import {
    faSearch, faCloudUploadAlt, faBell, faHeart, faCommentDots, faBookmark
} from '@fortawesome/free-solid-svg-icons';

@Component({
    selector: 'app-right-aside',
    templateUrl: './right-aside.component.html',
    styleUrls: ['./right-aside.component.scss']
})
export class rightAsideComponent implements OnInit {
    faSearch = faSearch;
    faCloudUploadAlt = faCloudUploadAlt;
    faBell = faBell;
    faHeart = faHeart;
    faCommentDots = faCommentDots;
    faBookmark = faBookmark;


    ngOnInit() {
    }
}